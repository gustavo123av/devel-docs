@startuml

autonumber
actor Jogador
boundary Cliente

Servidor -> Servidor: Pega os vencedores e prepara para enviar para o cliente.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: winlist  List{< type >< nickname >;< points >}� em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Monta a lista de Vencedores.
Cliente -> Jogador: Exibe a lista de vencedores.
@enduml

@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Conecta no jogo, informando o nickname.
Cliente -> Cliente: Prepara o comando: tetrisstart < nickname > < version > para enviar para o servidor
Cliente -> Servidor: Envia o comando codificado e em hexadecimal.
Servidor -> Servidor: Verifica se o nickname est� disponivel.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: playernum [..args] playerjoin [..args] winlist  [..args] team [..args] em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Coloca o usu�rio no time definido
Cliente -> Cliente : Coloca o nickname definido
Cliente -> Cliente : Coloca o o id do Jogador definido
Cliente -> Jogador : Notifica no batepapo que o jogador conectou no servidor.

@enduml

﻿

#  Requisito 2 - Iniciar Partida
## 1 - Resumo
**História de Usuário**
>Como moderador, gostaria de iniciar uma partida multiplayer

**Pré-condições**<br />
>O jogador é o moderador (#1) e pode emitir o comando start, caso tenham 2 ou mais jogadores na sala

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. A partida com os adversários irá começar, ou seja, as peças irão cair para o moderador e para os adversários que aparecem no campo de jogo dos adversários, com seus respectivos números e nicks.<br/>

>2. **Fluxo Alternativo** <br/>
	2.1 - Se um campo de um adversário deixar de exibir o número de identificação, o nick e o as peças do jogador adversário, então esse jogador não está mais na partida, se desconectou. Se todos adversários se desconectarem, terá que jogar sozinho. <br/>


**Observações**
>  

Caso não haja mais que 2 dois jogadores para iniciar uma partida multiplayer, ou caso todos os adversários se desconectem, o jogador 1 irá jogar sozinho. Se os jogadores se reconectarem, terão que esperar o jogador 1 terminar sua partida solo. Link do protocolo: https://gitlab.com/tetrinetjs/protocol/-/jobs/200969009/artifacts/file/tetrinetProtocol.pdf

## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **startgame < start >< iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- start :number => Se inicia ou finaliza o jogo (1 = inicia ou 0 = finaliza);<br/>
> --------- iDJogador:number => Id do jogador;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>4542656d4040d746ebe87ae1553178e13fcfc5efe36ae0acbe0a737461727467616d6520302031ff<br/>
> --------- em **Texto imprimivel**: startgame 1 1ÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **newgame < alturaInicial > < nivelInicial > < linhasNivel> < nivelIncremento> < especialLinha > < adicionarEspecial > < capacidadeEspecial > < frequenciaBlocos > < frequenciaEspecial > < nimelMedio > < modoClassico > f < iDJogador > < campos >**<br/>
> --- **Parâmetros**: <br/>
> --------- alturaInicial:number =>   
o número de linhas de lixo que devem estar presentes na parte inferior ou no campo de cada jogador quando o jogo é iniciado.;<br/>
> --------- nivelInicial:number => o número de linhas que um jogador deve limpar para avançar para o próximo nível;<br/>
> --------- linhasNivel:number => o número de níveis que um jogador avança quando preenche o número necessário de linhas;<br/>
> --------- especialLinha:number =>   
o número de linhas que um jogador deve eliminar antes que especiais sejam adicionadas ao campo dele;<br/>
> --------- adicionarEspecial:number => o número de especiais adicionados a partida de um jogador quando ele completar o número necessário de linhas;<br/>
> --------- capacidadeEspecial:number =>   
o número de specials que um jogador pode ter em sua fila em um determinado momento. Especiais coletados além desse número são descartados;<br/>
> --------- frequenciaBlocos:String => a frequência da ocorrência (número de earch sendo 1%) de cada um dos diferentes tipos de blocos;<br/>
> --------- frequenciaEspecial:String => a frequência de ocorrência de cada um dos diferentes blocos especiais;<br/>
> --------- nivelMedio:number => se 1, o cliente deve exibir os níveis médios de todos os jogadores no jogo (0 significa apenas que o valor está oculto);<br/>
> --------- modoClassico:number =>  se 1, os estilos clássico estão ativados;<br/>
> --------- idJogador:number => Numero identificador do jogador;<br/>
> --------- campos:string =>  o conteúdo do campo do jogador ou uma atualização parcial de campo;<br/>
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 4575dc4040c67ae1ebeef8a5725dcf5effcae062c6a9e062796e657767616d652030203120322031203120312031382031313131313131313131313131313232323232323232323232323232333333333333333333333333333333343434343434343434343434343435353535353535353535353535353636363636363636363636363636373737373737373737373737373737203131313131313131313131313131313131313232323232323232323232323232323232323333333434343434343434343434343636363636363636363636363636363637373738383838383838383838383839393939393939393939393939393939393920312031<br/>
 > a66203120303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030<br />
> --------- em **Texto imprimivel**: newgame 0 1 2 1 1 1 18 1111111111111122222222222222333333333333333444444444444445555555555555566666666666666777777777777777 1111111111111111112222222222222222223334444444444446666666666666666777888888888888999999999999999999 1 1 <br />
f 1 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000<br/>
## 3 - Diagrama de sequência

![Requisito 2](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%202/Requisito%202%20-%20IniciarPartida.png)




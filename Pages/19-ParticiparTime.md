﻿#  Requisito 19 - Participar de um time
## 1 - Resumo
**História de Usuário**
>  Como jogador, gostaria de participar de um time.

**Pré-condições**<br />
>1.1. Logar no jogo.

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O usuário escolhe em qual time gostaria de estar participando;<br/>

>2. **Fluxo Alternativo** <br/>
    2.1. Caso não escolha nenhum time, o jogo o coloca em um time default (Sozinho).<br/>    


**Observações**
>O Jogador pode se juntar a algum time, para realizar competições de times.
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **team < iDJogador > < team >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador;<br/>
> --------- team:string => Nome do time que o jogador quer participar;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 7465616d20312054696d655665726d656c686f**ff**(Adicionar o caracter 'ff')<br/>
> --------- em **Texto imprimivel**: team 1 TimeVermelhoÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **team < iDJogador > < team >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador;<br/>
> --------- team:string => Nome do time que o jogador quer participar;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 7465616d20312054696d655665726d656c686f<br/>
> --------- em **Texto imprimivel**: team 1 TimeVermelho<br/>
## 3 - Diagrama de sequência

![Requisito 19](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2019/Requisito%2019%20-%20Participar%20de%20Time.png)

